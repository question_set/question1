package com.mycompany.question1;

import java.util.ArrayList;
import java.util.List;

/**
 * Functions class have a method sortWords 
 * @author fatih
 */

public class Functions {

    /**
     *
     * sortWords method sorts ​a ​bunch ​of ​words ​by ​the ​number ​of
     * ​character ​“a”s within ​the ​word ​(decreasing ​order)
     * 
     * @param words- a string list
     * @return List has sorted words List.
     *
     */
    public static List<String> sortWords(List<String> words) {

        List<String> sortedList = new ArrayList<String>();
        while (!words.isEmpty()) {
            String bigger = "";
            int biggerCount = 0;

            for (String word : words) {

                int count = word.length() - word.replace("a", "").length();

                if (count > biggerCount) {
                    bigger = word;
                    biggerCount = count;
                }
                if (count == biggerCount) {
                    if (word.length() > bigger.length()) {
                        bigger = word;
                        biggerCount = count;
                    }
                }
            }

            sortedList.add(bigger);
            words.remove(bigger);

        }
        return sortedList;
    }

}

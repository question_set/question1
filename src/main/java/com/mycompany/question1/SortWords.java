package com.mycompany.question1;

import java.util.ArrayList;
import java.util.List;

/**
 * SortWords class is a main class
 * @author fatih
 * main class uses Functions class's sortWords function for sort words in an array list
 */
public class SortWords 
{
    public static void main( String[] args )
    {
        
        //creating list
        List<String> words = new ArrayList<String>();

        //adding words to list
        words.add("aaaasdf");
        words.add("aaaasd");
        words.add("a");
        words.add("aab");
        words.add("aaabcd");
        words.add("ef");
        words.add("cssssssd");
        words.add("fdz");
        words.add("kf");
        words.add("zc");
        words.add("lklklklklklklklkl");
        words.add("l");
        
        //word list is sending as a parameter  functions class's sortWords function for sorting
        //and the result is written on the screen
        System.out.println(Functions.sortWords(words));
    }
}

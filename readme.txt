This application;
has been developed for question1 is that below,
is a maven project you seen pom.xml for dependencies,
has a test class,
has a document(created with javadoc in folder \target\site\apidocs\index.html)


Question1:
Write a function that sorts a bunch of words by the number of character �a�s
within the word (decreasing order). If some words contain the same amount of
character �a�s then you need to sort those words by their lengths.
Input:
["aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"]
Output:
[aaaasd, aaabcd, aab, a, lklklklklklklklkl, cssssssd, fdz, ef, kf, zc, l]